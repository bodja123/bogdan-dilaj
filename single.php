   
<br/>
   
<br/>

    <div class="row">
        
        
        
        
        
        
         <div class="col-xs-12 col-md-8">

<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header>
        <div class="row">
    <div class="col-xs-12 col-md-8">
        <?php the_post_thumbnail( array(400, 400), array( 'class' => '  box-shad-sm' ) ); ?></div><div class="col-xs-12 col-md-4">
        <?php get_template_part('templates/entry-meta'); ?>   <h1 class="entry-title"><?php the_title(); ?></h1></div></div>
    </header>
      <br/>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
 
        </div>
        
        
         <div class="col-xs-12 col-md-4">
                          <form class="navbar-form" role="search" action="<?php echo site_url('/'); ?>" method="get" >
        <div class="input-group col-xs-12">
            <input type="text" class="form-control borderround serarchinpunt" placeholder="Suche" name="s" id="search" value="<?php the_search_query(); ?>">
            <div class="input-group-btn" style="padding-left:10px;">
                <button class="btn btn-default borderless iconop borderround" type="submit"><i class="glyphicon glyphicon-search iconop"></i></button>
            </div>
        </div>
</form>
             </br>
<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-newspaper-o "> </i></a></li>

    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-calendar"> </i></a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-comments"> </i></a></li>
        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-folder-open"> </i></a></li>

  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active " id="home"> <?php require_once("templ/news.php"); ?></div>
    <div role="tabpanel" class="tab-pane" id="profile"> <?php require_once("templ/events.php"); ?></div>
    <div role="tabpanel" class="tab-pane" id="messages"> <?php require_once("templ/comments.php"); ?></div>
    <div role="tabpanel" class="tab-pane" id="settings"> <?php require_once("templ/categories.php"); ?></div>
  </div>

</div>
        
             
            </div>
        </div>
</div>