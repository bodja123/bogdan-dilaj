 <div class="col-sm-6 col-md-4">
     <div  align="center">
<a href="<?php the_permalink(); ?>"><?php 
if ( has_post_thumbnail() ) { 
  the_post_thumbnail('thumbnail', array( 'class' => 'img-circle grayscale box-shad' ));
} 
    ?></a></div>

<article <?php post_class(); ?>>
  <header>
  <h3 class="caption" align="center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
 <?php get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
  
</article>
</div>