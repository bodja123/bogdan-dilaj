

<header class="banner">
    
  <div class="container">

   <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
  <div class="container">
        
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
            <a  role="button" data-toggle="collapse" href="#collapseInfo" aria-expanded="false" aria-controls="collapseExample">
<span class="btn btn-lg btn-bogdan  visible-xs-inline-block "><span class=" glyphicon glyphicon-info-sign blink "></span> BOGDAN DILAJ </span>
</a>
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
<script type="text/javascript">

     jQuery(document).ready(function($) {
    if (
   
    $(document).width() > 767 ) {
            $('.collapse').addClass('in');
        } 
     
     

     
     });
    
    
    
    
    
    
    
</script>
      
      
   
    </div>
<div style="margin-right:30px;">
        <?php
            wp_nav_menu( array(
                'menu'              => 'primary',
                'theme_location'    => 'primary',
                'depth'             => 3,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
        'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav navbar-right ',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?></div>
    </div>
</nav>
      </div>
      <div class="container ">
    <div class="collapse " id="collapseInfo">
<div class="jumbotron centered borderround jumboglow">
    
 <div class="row centered ">
  <div class="col-xs-12 col-sm-3 col-md-3">
    <div class="thumbnail img-circle" >
      <img src="<?= get_template_directory_uri(); ?>/dist/images/img_bogdan.jpg" alt="Bogdan Dilaj Portrait">
      
      </div></div>
   <div class="col-xs-12 col-sm-3 col-md-3">
    
   <?php
$post_id = 10;
$queried_post = get_post($post_id);
$title = $queried_post->post_title;
echo "<h3>",$title,"</h3>";
echo $queried_post->post_content;
?>
         
      
 </div>
     
     
     <div class="col-xs-6 col-sm-3 col-md-3">
     
     <!-- Button trigger modal -->
<button type="button" class="btn btn-default btn-lg btn-block  fa fa-envelope" data-toggle="modal" data-target="#myModal">
 <span class="text-muted">Kontakt</span>
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Kontaktieren Sie mich...</h4>
      </div>
      <div class="modal-body">
 	<form role="form" method="GET" id="contact-form" class="contact-form"action="<?php echo $_SERVER['PHP_SELF']; ?>">
                    
                            <input type="email" class="form-control" name="email" autocomplete="off" id="email" placeholder="Ihre e-mail Adresse">
                  	<br/>
                  		
                            <textarea class="form-control textarea" rows="3" name="Message" id="Message" placeholder="Ihre Nachricht"></textarea>
          
                 
              
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
        <button type="submit" class="btn btn-primary">Senden</button>   </form>
      </div>
    </div>
  </div>
</div>
          <?php
    if(isset($_GET['email'])) 
{ 
$to      = 'bdilay@gmail.com';
$subject = 'Kontaktierung via bogdan-dilaj.com';
$message = $_GET["Message"];
$headers = 'From: '.$_GET["email"] . "\r\n" .
    'Reply-To:'.$_GET["email"] . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

wp_mail($to, $subject, $message, $headers);
    echo '<div class="alert alert-success" role="alert">Ihre Anfrage wurde versendet!</div>';}
?> 
     
         <br/>
            <br/>
<span class="fa fa-phone "> </span>&nbsp;&nbsp; (+49) 08171 / 42 68 445
         
       
     
     </div> 
     
     
     
     
     
     
     
     
     
     
     <div class="col-xs-6 col-sm-3 col-md-3 centered">  
         
         <a class="btn btn-block btn-social btn-google" href="https://plus.google.com/+BogdanDilaj"  target="_blank" >
    <span class="fa fa-google-plus"></span>  <span class="smallertext">Google+ </span>
  </a>
         
            <a class="btn btn-block btn-social btn-twitter"  target="_blank" href="https://twitter.com/intent/follow?screen_name=BogdanDilaj" >
    <span class="fa fa-twitter"></span> <span class="smallertext"> Twitter </span>
  </a>     
                     <a class="btn btn-block btn-social btn-facebook"  target="_blank"  href="https://www.facebook.com/bogdan.dilaj">
    <span class="fa fa-facebook"></span>  <span class="smallertext"> Facebook </span>
  </a> 
                     <a class="btn btn-block btn-social btn-reddit"  target="_blank"  href="https://www.xing.com/profile/Bogdan_Dilay?sc_o=mxb_p">
    <span class="fa fa-xing"></span>  <span class="smallertext"> Xing </span>
  </a> 
                         <a class="btn btn-block btn-social btn-bitbucket"  target="_blank"  href="https://bitbucket.org/bodja123/">
    <span class="fa fa-bitbucket"></span>  <span class="smallertext"> Bitbucket </span>
  </a> 
                                <a class="btn btn-block btn-social btn-github"  target="_blank"  href="https://github.com/doctor-damager">
    <span class="fa fa-github"></span> <span class="smallertext">  Github </span>
  </a> 
                                     <a class="btn btn-block btn-social btn-soundcloud"  target="_blank"  href="https://soundcloud.com/bogdan-dilaj">
    <span class="fa fa-soundcloud"></span> <span class="smallertext">  Soundcloud </span>
  </a> 
                                     <a class="btn btn-block btn-social btn-pinterest"  target="_blank"  href="https://www.youtube.com/user/BDTVEntertainment">
    <span class="fa fa-youtube"></span>  <span class="smallertext"> Youtube </span>
  </a> 

   
     
     </div>
  </div>

    

 </div>

  </div>
</div>
  </div>
    
   

</header>
