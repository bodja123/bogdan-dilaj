
<div class="list-group">
     <span href="#" class="list-group-item borderless">

  </span>

<?php


$args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category' => 5 );

$myposts = get_posts( $args );
foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
	
		<a class="list-group-item borderless" href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(40, 40), array( 'class' => 'img-circle grayscale box-shad-sm' ) ); ?> &nbsp;<?php the_title(); ?></a>
	
<?php endforeach; 
wp_reset_postdata();?>


</div>